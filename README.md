# urban-electric

This repo was created within 24 hours of a job interview for C#. I did not have any recent experience so I setup a project and actually presented it during the interview. I did not get the job but it was fun to put in the work on a time crunch. I was proud of the result.

## Getting started

For initial development advice and standards, I always refer to the [documentation](https://docs.microsoft.com/en-us/dotnet/) first.

Hello World sandbox available at [Microsoft](https://docs.microsoft.com/en-us/learn/modules/csharp-write-first/2-exercise-hello-world).

Setting up development environment for MVC [web apps](https://docs.microsoft.com/en-us/aspnet/core/tutorials/first-mvc-app/start-mvc?view=aspnetcore-6.0&tabs=visual-studio-code).

### Steps Taken

1. Install C# for Visual Studio code
2. Download [Dotnet SDK](https://dotnet.microsoft.com/en-us/download/dotnet/thank-you/sdk-6.0.400-windows-x64-installer)
3. Open integrated terminal and type: 

```
dotnet new mvc -o UrbanElectricMvc
code -r UrbanElectricMvc
```
**This creates a new ASP.NET Core MVC Project named UrbanElectricMvc**

4. Create a launch.json file for testing and debugging. 
5. Application is up and running on localhost:7110


![demo](hello_world.PNG)


6. Create extremely basic [controller](https://docs.microsoft.com/en-us/aspnet/core/tutorials/first-mvc-app/adding-controller?view=aspnetcore-6.0&tabs=visual-studio-code)

*Reference /[Controller]/[ActionName]/[Parameters]*

```
app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");
```


7. Create [views](https://docs.microsoft.com/en-us/aspnet/core/tutorials/first-mvc-app/adding-view?view=aspnetcore-6.0&tabs=visual-studio-code) and pass data from controller.

8. Create [model](https://docs.microsoft.com/en-us/aspnet/core/tutorials/first-mvc-app/adding-model?view=aspnetcore-6.0&tabs=visual-studio-code) and setup scaffolding for model and database.

9. Connect to SQLite db for testing. Crud App is up and running, tested and working.

![demo](crud.PNG)

10. Include threejs project into .NET core application. 
Tested and working. 

Problem: Need to find a way to separate css and js files in layout depending on pages. Currently, threejs background shows on every page.

![demo](threejs.PNG)

## Name
Urban Electric Demo.

## Description
A simple application setup with .NET Core and developed rapidly for demo in interview.

## Usage
Free to download and share with peers. 

## Roadmap
Develop a simple application and API using .NET Core. Create an application that hosts a 3D model from threejs.

## Project status

- [x] [Starting Initial Setup]()
- [x] [Create Test App "Hello World"]()
- [x] [Setup Basic API]()
- [x] [Create Front-End]()
- [x] [Connect Application]()
- [x] [Test Front & Back-End]()
- [x] [Add 3D Model with threejs]()
- [ ] [Deploy Application]()
- [ ] [Test Application]()


## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.
## Contributors ✨

<a href="https://b3po.io"><img src="https://b3po.io/resume/assets/images/avi-bkgd.jpg" width="100px;" alt=""/><br /><sub><b>B3PO</b></sub></a><br /><a href="https://b3po.io/resume/contact.html" title="Contact B3PO">💬</a>

## License
MIT

