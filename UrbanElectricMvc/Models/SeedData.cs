using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
//using UrbanElectricMvc.Data;
using System;
using System.Linq;

namespace UrbanElectricMvc.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new MvcProductContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<MvcProductContext>>()))
            {
                // Look for any movies.
                if (context.Product.Any())
                {
                    return;   // DB has been seeded
                }

                context.Product.AddRange(
                    new Product
                    {
                        Title = "Posy Flush",
                        ReleaseDate = DateTime.Parse("2022-1-1"),
                        Category = "Lighting",
                        Price = 1176
                    },

                    new Product
                    {
                        Title = "Hyde ",
                        ReleaseDate = DateTime.Parse("2022-1-2"),
                        Category = "Lighting",
                        Price = 1125
                    },

                    new Product
                    {
                        Title = "Chisholm Clean",
                        ReleaseDate = DateTime.Parse("2022-2-1"),
                        Category = "Lighting",
                        Price = 4685
                    },

                    new Product
                    {
                        Title = "Disc",
                        ReleaseDate = DateTime.Parse("2022-3-1"),
                        Category = "Lighting",
                        Price = 6635
                    }
                );
                context.SaveChanges();
            }
        }
    }
}